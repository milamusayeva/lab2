# 1.Application development

## FastAPI with SQLAlchemy and MySQL
This is a basic CRUD (Create, Read, Update, Delete) application using FastAPI, SQLAlchemy, and MySQL. The application is containerized with Docker for easy setup and deployment.

## Application link -> http://129.159.207.195/docs

## Prerequisites

Before you begin, ensure you have met the following requirements:
- You have installed Docker and Docker Compose.
- You have basic knowledge of Python, FastAPI, SQLAlchemy, and Docker.

# 2.Dockerizing the application

## Dockerfile for a Python Application

This `Dockerfile` sets up a Python application with Uvicorn, an ASGI server, running on port 80.

```Dockerfile
# Use an official Python runtime as a parent image
FROM python:3.11

# Set the working directory in the container
WORKDIR /code

# Copy the requirements file into the container at /code/requirements.txt
COPY app/requirements.txt /code/requirements.txt

# Install any dependencies in the requirements file
RUN pip install -r /code/requirements.txt

# Copy the current directory contents into the container at /code/app
COPY ./app /code/app

# Run the application using Uvicorn
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
``` 

- `FROM python:3.11` - This line indicates that the base image will be Python version 3.11, which is an official Python Docker image.
- `WORKDIR /code` - Sets the working directory in the container to `/code`. Any subsequent commands will be run from this directory.
- `COPY app/requirements.txt /code/requirements.txt` - Copies the `requirements.txt` file from the local app directory to the `/code` directory in the container. This file lists the Python dependencies the application needs.
- `RUN pip install -r /code/requirements.txt` - Installs the Python dependencies specified in `requirements.txt`.
- `COPY ./app /code/app` - Copies the entire app


## Docker Compose
This configuration defines a multi-container Docker environment with two services: a `db` service for MySQL and an `app` service for the application.

```yaml
version: '3.10'

services:
  db:
    image: mysql:latest
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: test
      MYSQL_DATABASE: user_db
    volumes:
      - ./mysql_data:/var/lib/mysql
      - ./sql-scripts:/docker-entrypoint-initdb.d
    ports:
      - "3306:3306"
    networks:
      - mynetwork
    healthcheck:
      test: [ "CMD", "mysqladmin", "ping", "-h", "localhost" ]
      interval: 10s
      timeout: 5s
      retries: 5

  app:
    build: .
    ports:
      - "80:80"
    depends_on:
      db:
        condition: service_healthy
    networks:
      - mynetwork

networks:
  mynetwork:
    driver: bridge
```

- **Image**:
  - `image: mysql:latest`: Uses the latest MySQL image.

- **Restart Policy**:
  - `restart: always`: Ensures that the container always restarts if it stops.

- **Environment Variables**:
  - `environment`: Sets environment variables in the container for the MySQL root password (`MYSQL_ROOT_PASSWORD`) and database name (`MYSQL_DATABASE`).

- **Volumes**:
  - Maps `./mysql_data` on the host to `/var/lib/mysql` in the container to persist database data.
  - Maps `./sql-scripts` to `/docker-entrypoint-initdb.d` to execute initialization SQL scripts.

- **Ports**:
  - `ports`: Maps port `3306` on the host to port `3306` in the container, exposing the MySQL service.

- **Networks**:
  - `networks`: Attaches the container to the `mynetwork` network.

## Building image
For running application in local environmet we need to:
```text
docker compose up 
```
And application will be available at http:\\127.0.0.1:80
# 3. Gitlab setup

You can clone this repository simply by copying commands below:
```text
git clone https://gitlab.com/milamusayeva/lab2.git
cd lab2 
```
As task needs docker, you don't need anything further for dependencies. But if network
```text
pip install -r requirements.txt
```

# 4. Gitlab CI/CD configuration x Deployment with Docker

## GitLab CI/CD Pipeline Configuration
Stages in the Pipeline
The pipeline is composed of three stages:

1. `build`
2. `test`
3. `deploy`

## Build Stage

```yaml
build:
  stage: build
  tags:
    - lab2
  script:
    - docker compose build
  only:
    - main
```
- **Stage**: `build`
- **Tags**: `lab2` - The runner tag to be used.
- **Script**: Executes `docker compose build` to create Docker images.
- **Branch**: This job runs only on the `main` branch.

## Test stage
```yaml
test:
  stage: test
  tags:
    - lab2
  script:
    - docker compose run app pytest /code/app/app_tests.py
    - docker compose down
  only:
    - main
```
- **Stage**: `test`
- **Tags**: `lab2` - Specifies the runner tag.
- **Script**:
  - Runs tests using `pytest` inside the app service.
  - Shuts down the setup using `docker compose down`.
- **Branch**: Restricts the job to the `main` branch.

## Deploy stage
```yaml
deploy:
  stage: deploy
  tags:
    - lab2
  script:
    - docker compose up -d
  only:
    - main
```
- **Stage**: `deploy`
- **Tags**: `lab2` - Designates the runner tag.
- **Script**: Deploys the application in detached mode using `docker compose up -d`.
- **Branch**: Limited to the `main` branch.

# 6. Testing
For testing I've written 4 testcases in python which can be accessed from /app directory
Sample one:
```python
@pytest.mark.asyncio
async def test_create_user_endpoint():
    user_data = {"username": "testuser", "password": "testpass"}
    response = client.post("/users/", json=user_data)
    assert response.status_code == 200
    created_user = response.json()
    assert created_user["username"] == user_data["username"]
```
## Below is the successful result of the pipeline

![Alt text](image.png)