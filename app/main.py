from fastapi import FastAPI,HTTPException
import sys
sys.path.append("/code/app")
from models import Base,User
from pydantic import BaseModel
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import pymysql

engine = create_engine("mysql+pymysql://root:test@db:3306/user_db")
Session = sessionmaker(bind=engine)

def recreate_database():
    #Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

recreate_database()

app = FastAPI()

class UserCreate(BaseModel):
    username: str
    password: str

class UserUpdate(BaseModel):
    username: str
    password: str

class UserOut(BaseModel):
    id: int
    username: str

    class Config:
        orm_mode = True

def get_user(db_session, user_id: int):
    return db_session.query(User).filter(User.id == user_id).first()

def get_user_by_username(db_session, username: str):
    return db_session.query(User).filter(User.username == username).first()

def create_user(db_session, user: UserCreate):
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = User(username=user.username, password=fake_hashed_password)
    db_session.add(db_user)
    db_session.commit()
    db_session.refresh(db_user)
    return db_user

def update_user(db_session, user_id: int, user: UserUpdate):
    db_user = get_user(db_session, user_id)
    if db_user:
        db_user.username = user.username
        db_user.password = user.password + "notreallyhashed"
        db_session.commit()
        db_session.refresh(db_user)
        return db_user
    else:
        raise HTTPException(status_code=404, detail="User not found")

def delete_user(db_session, user_id: int):
    db_user = get_user(db_session, user_id)
    if db_user:
        db_session.delete(db_user)
        db_session.commit()
        return True
    else:
        raise HTTPException(status_code=404, detail="User not found")

@app.post("/users/", response_model=UserOut)
def create_user_endpoint(user: UserCreate):
    db = Session()
    db_user = get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    return create_user(db, user)

@app.get("/users/{user_id}", response_model=UserOut)
def read_user(user_id: int):
    db = Session()
    db_user = get_user(db, user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@app.put("/users/{user_id}", response_model=UserOut)
def update_user_endpoint(user_id: int, user: UserUpdate):
    db = Session()
    return update_user(db, user_id, user)

@app.delete("/users/{user_id}")
def delete_user_endpoint(user_id: int):
    db = Session()
    return delete_user(db, user_id)